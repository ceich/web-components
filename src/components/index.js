export { default as Example } from './example';

// @Deprecated
// Esto está únicamente para dejar contento a eslint.
// Cuando haya más de un componente, sacarlo
export { default as Example2 } from './example';
