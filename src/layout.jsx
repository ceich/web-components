import React from 'react';
import { render } from 'react-dom';
import Example from 'components/example';

render(<Example />, document.getElementById('root'));
