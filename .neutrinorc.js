const airbnb = require('@neutrinojs/airbnb');
const reactComponents = require('@neutrinojs/react-components');
const jest = require('@neutrinojs/jest');

const { join } = require('path');

module.exports = {
  options: {
    root: __dirname
  },
  use: [
    airbnb({
      eslint: {
        useEslintrc: true
      }
    }),
    reactComponents(),
    jest(),
    neutrino => {
      if (process.env.NODE_ENV !== 'production') {
        neutrino.config.entry['index'] = join(__dirname, 'src/layout');
      }
    }
  ],
};
